# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-

# engage ol damping
import susconst
#susconst.use_olDamp = True
#susconst.ol_off_threshold = 800
#susconst.ol_on_threshold = 400

# import the base SUS module
from SUS import *

prefix = 'SUS-BS'

##################################################
# SVN $Id$
# $HeadURL$
##################################################
